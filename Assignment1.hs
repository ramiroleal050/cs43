import Data.List
import Data.Ratio


-- Problem 1

myMap :: (a -> b) -> [a] -> [b]
myMap f xs        = foldr step [] xs
  where step y ys = (f y):ys


-- Problem 4
-- In > evenDivBy [1..20]
-- Out> 232792560

evenDivBy :: [Int] -> Int
evenDivBy             = product . foldr step []
  where
    step a acc        = shedCommonFactors a acc:acc
    shedCommonFactors = foldl step'
      where step' a b = denominator $ b % a


-- Problem 5
-- In > prime 10001
-- Out> 104743

prime :: Int -> Int
prime n             = head $ drop (n - 1) $ foldr step [] [2..]
  where
    step a acc
      | isPrime a   = a:acc
      | otherwise   = acc
    isPrime x       = and $ map ((/=) 0 . (mod x)) [2..rootOfx]
      where rootOfx = floor $ sqrt $ fromIntegral x