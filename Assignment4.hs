module Main where

import Data.Char
import Control.Applicative
import Parser (Parser(..), item, satisfy, char, notChar, digit, spaces)

main :: IO ()
main = putStrLn "hi"


data Expr = Num Int
          | Neg Expr
          | Add [Expr]
          | Mul [Expr]
          deriving (Show)


eval :: Expr -> Int
eval (Num x) = x
eval (Neg x) = - eval x
eval (Add xs) = sum $ map eval xs
eval (Mul xs) = product $ map eval xs


spaceChar :: Char -> Parser Char
spaceChar c = spaces *> (char c) <* spaces

inParens :: Parser a -> Parser a
inParens p = spaces *> char '(' *> p <* char ')' <* spaces

number :: Parser Expr
number = Num . read <$> (spaces *> some digit <* spaces)

neg :: Parser Expr
neg = Neg <$> ((spaceChar '-') *> expr)
  
add :: Parser Expr
add = Add <$> ((spaceChar '+') *> some expr)

mul :: Parser Expr
mul = Mul <$> ((spaceChar '*') *> some expr)

expr :: Parser Expr
expr = number <|> inParens (neg <|> add <|> mul)

interpret :: String -> Maybe Int
interpret s = case getParser expr s of
                []     -> Nothing
                (x:xs) -> Just $ eval $ fst x -- evaluate the first parsed expression
                -- note that with this parser only one possible parse is
                -- possible, and therefore a successful parse will always give a
                -- list of length 1
