module Main where

import Data.Semigroup (stimes) -- stimes not imported by default
import System.Environment (getArgs)

main :: IO ()
main = do
  putStrLn "Hello world!"


newtype Product = Product Int deriving (Show)
newtype Sum = Sum Int deriving (Show)


instance Semigroup Sum where
    Sum a <> Sum b = Sum $ a + b
    
    stimes n (Sum x) = Sum $ fromIntegral n * x -- O(1) stimes

    
instance Semigroup Product where
    Product a <> Product b = Product $ a * b
    
    stimes n (Product x) = Product $ x^n -- O(1) stimes


stimes' :: (Semigroup a, Integral b) => b -> a -> a
stimes' n x
  | n <= 0    = error "positive multiplier expected"
  | n == 1    = x
  | even n    = let xs = stimes' (div n 2) x in xs <> xs
  | otherwise = x <> stimes' (n - 1) x
