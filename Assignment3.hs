---------------
-- Problem 1 --
---------------

data MyEither a b = MyLeft a | MyRight b
  deriving (Show)

instance Functor (MyEither e) where
  fmap f (MyLeft e) = MyLeft e
  fmap f (MyRight b) = MyRight $ f b

---------------
-- Problem 2 --
---------------  

data Tree a = Node a [Tree a]
  deriving (Show)

instance Functor Tree where
  fmap f (Node a []) = Node (f a) []
  fmap f (Node a ts) = Node (f a) (fmap (fmap f) ts)

---------------
-- Problem 3 --
---------------

{-
prop. The composition of two Functors is another Functor.

It suffices to show that an fmap can be defined satisfying
the laws of Functors.

Let A and B be two Functors. We can define a new type

newtype C a = C {getC :: A (B a)}

Then, for this type let

fmap f = C . fmap (fmap f) . getC

where the rightmost fmap is defined in the Functor instance
of B, and the fmap right before it is defined in the Functor
instance of A. 

fmap id == C . fmap (fmap id) . getC
        == C . fmap id . getC
        == C . id . getC
        == C . getC
        == id

fmap f . fmap g == C . fmap (fmap f) . getC . C . fmap (fmap g) . getC
                == C . fmap (fmap f) . fmap (fmap g) . getC
                == C . fmap ((fmap f) . (fmap g)) . getC
                == C . fmap (fmap (f . g)) . getC
                == fmap (f . g)

Q.E.D.
-}
